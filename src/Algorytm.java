public class Algorytm {

    final static double mutationRate = Main.mutationRate;
    final static double crossoverRate = Main.crossoverRate;
    final static int tournamentSize = Main.tournamentSize;
    final static boolean elitism = Main.elitism;

    public static Populacja evolvePopulation(Populacja pop) {
        Populacja newPopulation = new Populacja(pop.populationSize(), false);

        int elitismOffset = 0;
        if (elitism) {
            newPopulation.saveTour(0, pop.getFittest());
            elitismOffset = 1;
        }

            for (int i = elitismOffset; i < newPopulation.populationSize(); i++) {
                    Trasa parent1 = tournamentSelection(pop);
                    Trasa parent2 = tournamentSelection(pop);
                    if(Math.random() < crossoverRate); Trasa child = crossover(parent1, parent2);
                    newPopulation.saveTour(i, child);
            }

        for (int i = elitismOffset; i < newPopulation.populationSize(); i++)mutate(newPopulation.getTour(i));
        return newPopulation;
    }

    public static Trasa crossover(Trasa parent1, Trasa parent2) {
        Trasa child = new Trasa();

        int startPos = (int) (Math.random() * parent1.tourSize());
        int endPos = (int) (Math.random() * parent1.tourSize());

        for (int i = 0; i < child.tourSize(); i++) {
            if (startPos < endPos && i > startPos && i < endPos) child.setCity(i, parent1.getCity(i));
            else if (startPos > endPos)
                if (!(i < startPos && i > endPos)) child.setCity(i, parent1.getCity(i));
        }

        for (int i = 0; i < parent2.tourSize(); i++) {
            if (!child.containsCity(parent2.getCity(i))) {
                for (int ii = 0; ii < child.tourSize(); ii++) {
                    if (child.getCity(ii) == null) {
                        child.setCity(ii, parent2.getCity(i));
                        break;
                    }
                }
            }
        }
        return child;
    }

    private static void mutate(Trasa tour) {
        for(int tourPos1=0; tourPos1 < tour.tourSize(); tourPos1++){
            if(Math.random() < mutationRate){
                int tourPos2 = (int) (tour.tourSize() * Math.random());

                Miasto city1 = tour.getCity(tourPos1);
                Miasto city2 = tour.getCity(tourPos2);

                tour.setCity(tourPos2, city1);
                tour.setCity(tourPos1, city2);
            }
        }
    }

    private static Trasa tournamentSelection(Populacja pop) {
        Populacja tournament = new Populacja(tournamentSize, false);
        for (int i = 0; i < tournamentSize; i++) {
            int randomId = (int) (Math.random() * pop.populationSize());
            tournament.saveTour(i, pop.getTour(randomId));
        }
        Trasa fittest = tournament.getFittest();
        return fittest;
    }
}