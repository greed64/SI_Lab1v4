
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    final static String FILE_NAME = "D:\\Documents\\Desktop\\";
    final static Charset ENCODING = StandardCharsets.ISO_8859_1;

    public final static int populationSize = 100;
    public final static int generationSize =500;

    public static final double mutationRate = 0.0005;
    public static final double crossoverRate = 0.3;
    public static final int tournamentSize = 5;
    public static final boolean elitism = true;

    public static void main(String[] args) throws IOException {
        Path path = Paths.get(FILE_NAME+"output"+populationSize+".txt");
        File file = new File("a.tsp");
        int finalAvgBest=0, finalAvgWorst=0, finalAvgAvg = 0;

        try {

            Scanner sc = new Scanner(file);
            int temp=1;
            int x,y;
            while (sc.hasNextLine()) {
                if(sc.hasNextInt()) {
                    sc.nextInt();
                    x = (int)sc.nextDouble();
                    y = (int)sc.nextDouble();
                    System.out.println("Miasto: "+(temp-7)+"  X: "+x+"   Y: "+y);
                    ListaTras.addCity(new Miasto(x, y));
                }
                sc.nextLine();
                temp++;
            }
            sc.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("=========================================================");

        Populacja pop = new Populacja(populationSize, true);
        System.out.println("Initial distance: " + pop.getFittest().getDistance());
        BufferedWriter writer = Files.newBufferedWriter(path, ENCODING);

        writer.write(new String("MutationRate "+ Algorytm.mutationRate));
        writer.newLine();
        writer.write(new String("CrossoverRate "+ Algorytm.crossoverRate));
        writer.newLine();
        writer.write(new String("TournamentSize "+ Algorytm.tournamentSize));
        writer.newLine();
        writer.newLine();
        writer.write("Pop Best Avg Worst");
        writer.newLine();

        pop = Algorytm.evolvePopulation(pop);
        for (int i = 0; i < generationSize; i++) {
            pop = Algorytm.evolvePopulation(pop);
            System.out.println((i + 1) + " " + pop.getBest().getDistance() + " " + pop.getAverange() + " " + pop.getWorst().getDistance());
            writer.write(new String((i + 1) + " " + pop.getBest().getDistance() + " " + pop.getAverange() + " " + pop.getWorst().getDistance()));
            writer.newLine();
            finalAvgAvg+=pop.getAverange();
            finalAvgBest+=pop.getBest().getDistance();
            finalAvgWorst+=pop.getWorst().getDistance();
        }
        writer.flush();

        System.out.println("Finished");
        System.out.println("Best distance: " + pop.getFittest().getDistance());

        System.out.println("AVG AVG: "+finalAvgAvg/generationSize);

    }
}