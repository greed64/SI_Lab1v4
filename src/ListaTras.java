import java.util.ArrayList;

public class ListaTras {

    private static ArrayList destinationCities = new ArrayList<Miasto>();

    public static void addCity(Miasto city) {
        destinationCities.add(city);
    }

    public static Miasto getCity(int index){
        return (Miasto)destinationCities.get(index);
    }

    public static int numberOfCities(){
        return destinationCities.size();
    }
}