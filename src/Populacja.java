public class Populacja {
    Trasa[] trasa;

    public Populacja(int populationSize, boolean initialise) {
        trasa = new Trasa[populationSize];
        if (initialise) {
            for (int i = 0; i < populationSize(); i++) {
                Trasa newTrasa = new Trasa();
                newTrasa.generateIndividual();
                saveTour(i, newTrasa);
            }
        }
    }

    public Trasa getWorst(){
        Trasa fittest = trasa[0];
        for (int i = 1; i < populationSize(); i++)if (fittest.getFitness() >= getTour(i).getFitness())fittest = getTour(i);
        return fittest;
    }

    public Trasa getBest(){
        Trasa fittest = trasa[0];
        for (int i = 1; i < populationSize(); i++)if (fittest.getFitness() <= getTour(i).getFitness())fittest = getTour(i);
        return fittest;
    }

    public int getAverange(){
        int fittest = trasa[0].getDistance();
        for (int i = 1; i < populationSize(); i++)fittest += getTour(i).getDistance();
        fittest = fittest/populationSize();
        return fittest;
    }

    public Trasa getFittest() {
        Trasa fittest = trasa[0];
        for (int i = 1; i < populationSize(); i++)if(fittest.getFitness() <= getTour(i).getFitness())fittest = getTour(i);
        return fittest;
    }

    public void saveTour(int index, Trasa trasa) {
        this.trasa[index] = trasa;
    }

    public Trasa getTour(int index) {
        return trasa[index];
    }

    public int populationSize() {
        return trasa.length;
    }
}