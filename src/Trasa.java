import java.util.ArrayList;
import java.util.Collections;

public class Trasa {
    private ArrayList tour = new ArrayList<Miasto>();
    private double fitness = 0;
    private int distance = 0;

    public Trasa(){for(int i = 0; i < ListaTras.numberOfCities(); i++)tour.add(null);}

    public void generateIndividual() {
        for (int cityIndex = 0; cityIndex < ListaTras.numberOfCities(); cityIndex++)setCity(cityIndex, ListaTras.getCity(cityIndex));
        Collections.shuffle(tour);
    }

    public void setCity(int tourPosition, Miasto city) {
        tour.set(tourPosition, city);
        fitness = 0;
        distance = 0;
    }

    public Miasto getCity(int tourPosition) {
        return (Miasto)tour.get(tourPosition);
    }

    public double getFitness() {
        if (fitness == 0)fitness = 1/(double)getDistance();
        return fitness;
    }

    public int getDistance(){
        if (distance == 0) {
            int tourDistance = 0;
            for (int cityIndex=0; cityIndex < tourSize(); cityIndex++) {
                Miasto fromCity = getCity(cityIndex);
                Miasto destinationCity;
                if(cityIndex+1 < tourSize())destinationCity = getCity(cityIndex+1);
                else destinationCity = getCity(0);
                tourDistance += fromCity.distanceTo(destinationCity);
            }
            distance = tourDistance;
        }
        return distance;
    }

    public int tourSize() {
        return tour.size();
    }

    public boolean containsCity(Miasto city){
        return tour.contains(city);
    }
}